<?php

namespace App\Controller;

use App\Entity\Pizza;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes\Delete;
use OpenApi\Attributes\Get;
use OpenApi\Attributes\Items;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Post;
use OpenApi\Attributes\Put;
use OpenApi\Attributes\Schema;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api')]
class RestController extends AbstractFOSRestController
{
    protected EntityManagerInterface $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/pizza', name: 'pizza_list', methods: ['GET'])]
    #[Get(
        description: 'Fetches a list of pizzas.',
        tags: ['Pizza'],
        parameters: [
            new Parameter(
                name: 'token',
                description: 'Access token',
                in: 'query',
                required: true,
                schema: new Schema(
                    type: 'string'
                ),
                example: '3fa'
            ),
        ],
        responses: [
            new \OpenApi\Attributes\Response(
                response: 200,
                description: 'A set of pizza objects.',
                content: new JsonContent(
                    type: 'array',
                    items:   new Items(
                        ref: new Model(
                            type: Pizza::class
                        ),
                    ),
                ),
            ),
            new \OpenApi\Attributes\Response(
                response: 401,
                description: 'Unauthorized if empty or wrong token.',
                content: new JsonContent(
                    type: 'object',
                ),
            ),
        ]
    )]
    public function listAction(): Response
    {
        $pizzas = $this
            ->entityManager
            ->getRepository(Pizza::class)
            ->findAll();

        $view = $this->view($pizzas, Response::HTTP_OK);

        return $this->handleView($view);
    }

    #[Route('/pizza/{id}', name: 'pizza_details', methods: ['GET'])]
    #[Get(
        description: 'Fetch a pizza by it\'s id.',
        tags: ['Pizza'],
        parameters: [
            new Parameter(
                name: 'token',
                description: 'Access token',
                in: 'query',
                required: true,
                schema: new Schema(
                    type: 'string'
                ),
                example: '3fa'
            ),
        ],
        responses: [
            new \OpenApi\Attributes\Response(
                response: 200,
                description: 'Single pizza record details.',
                content: new JsonContent(
                    ref: new Model(
                        type: Pizza::class
                    ),
                    type: 'object',
                ),
            ),
            new \OpenApi\Attributes\Response(
                response: 401,
                description: 'Unauthorized if empty or wrong token.',
                content: new JsonContent(
                    type: 'object',
                ),
            ),
            new \OpenApi\Attributes\Response(
                response: 404,
                description: 'Pizza record with given id not found.',
                content: new JsonContent(
                    type: 'object',
                ),
            ),
        ]
    )]
    public function detailsAction(string $id): Response
    {
        $pizza = $this
            ->entityManager
            ->getRepository(Pizza::class)
            ->find($id);

        return $this->handleView(
            $pizza ? $this->view($pizza, Response::HTTP_OK) : $this->view([], Response::HTTP_NOT_FOUND)
        );
    }

    #[Route('/pizza', name: 'pizza_insert', methods: ['POST'])]
    #[Post(
        description: 'Create a new record.',
        tags: ['Pizza'],
        parameters: [
            new Parameter(
                name: 'token',
                description: 'Access token',
                in: 'query',
                required: true,
                schema: new Schema(
                    type: 'string'
                ),
                example: '3fa'
            ),
            new Parameter(
                name: 'name',
                description: 'Name of the pizza, at least 3 characters.',
                required: true,
                schema: new Schema(
                    type: 'string'
                ),
                example: 'Fungi'
            ),
            new Parameter(
                name: 'price',
                description: 'Price of the pizza. If set, should be greater than 0.',
                required: false,
                schema: new Schema(
                    type: 'integer'
                ),
                example: '20'
            ),
            new Parameter(
                name: 'glutenfree',
                description: 'Gluten free property.',
                required: false,
                schema: new Schema(
                    type: 'boolean'
                ),
            ),
            new Parameter(
                name: 'spicy',
                description: 'Spicy taste property.',
                required: false,
                schema: new Schema(
                    type: 'boolean'
                ),
            ),
            new Parameter(
                name: 'sweet',
                description: 'Sweet taste property.',
                required: false,
                schema: new Schema(
                    type: 'boolean'
                ),
            ),
            new Parameter(
                name: 'vegan',
                description: 'Vegan property.',
                required: false,
                schema: new Schema(
                    type: 'boolean'
                ),
            ),
            new Parameter(
                name: 'vegetarian',
                description: 'Vegetarian property.',
                required: false,
                schema: new Schema(
                    type: 'boolean'
                ),
            ),
        ],
        responses: [
            new \OpenApi\Attributes\Response(
                response: 201,
                description: 'Pizza record created.',
                content: new JsonContent(
                    ref: new Model(
                        type: Pizza::class
                    ),
                    type: 'object',
                ),
            ),
            new \OpenApi\Attributes\Response(
                response: 400,
                description: 'Pizza record data validation error.',
                content: new JsonContent(
                    type: 'object',
                ),
            ),
            new \OpenApi\Attributes\Response(
                response: 401,
                description: 'Unauthorized if empty or wrong token.',
                content: new JsonContent(
                    type: 'object',
                ),
            ),
            new \OpenApi\Attributes\Response(
                response: 409,
                description: 'Pizza record with same name exists.',
                content: new JsonContent(
                    type: 'object',
                ),
            ),
        ]
    )]
    public function insertAction(Request $request, ValidatorInterface $validator): Response
    {
        $name = $request->query->get('name', '');

        $newPizza = (new Pizza())
            ->setName($name)
            ->setPrice($request->query->get('price', 0))
            ->setGlutenfree($request->query->get('glutenfree', false))
            ->setSpicy($request->query->get('spicy', false))
            ->setSweet($request->query->get('sweet', false))
            ->setVegan($request->query->get('vegan', false))
            ->setVegetarian($request->query->get('vegetarian', false));

        $validationResults = $validator->validate($newPizza);
        if (count($validationResults) > 0) {
            return $this->handleView(
                $this->view((string) $validationResults, Response::HTTP_BAD_REQUEST)
            );
        }

        $pizza = $this
            ->entityManager
            ->getRepository(Pizza::class)
            ->findOneBy([
                'name' => $name
            ]);

        if ($pizza) {
            return $this->handleView(
                $this->view(['err' => 'Pizza with such name exists.'], Response::HTTP_CONFLICT)
            );
        }

        $this->entityManager->persist($newPizza);
        $this->entityManager->flush();

        return $this->handleView(
            $this->view($newPizza, Response::HTTP_CREATED)
        );
    }

    #[Route('/pizza/{id}', name: 'pizza_update', methods: ['PUT'])]
    #[Put(
        description: 'Update an existing pizza object.',
        tags: ['Pizza'],
        parameters: [
            new Parameter(
                name: 'token',
                description: 'Access token',
                in: 'query',
                required: true,
                schema: new Schema(
                    type: 'string'
                ),
                example: '3fa'
            ),
            new Parameter(
                name: 'name',
                description: 'Name of the pizza, at least 3 characters.',
                required: false,
                schema: new Schema(
                    type: 'string'
                ),
                example: 'Fungi'
            ),
            new Parameter(
                name: 'price',
                description: 'Price of the pizza. If set, should be greater than 0.',
                required: false,
                schema: new Schema(
                    type: 'integer'
                ),
                example: '20'
            ),
            new Parameter(
                name: 'glutenfree',
                description: 'Gluten free property.',
                required: false,
                schema: new Schema(
                    type: 'boolean'
                ),
            ),
            new Parameter(
                name: 'spicy',
                description: 'Spicy taste property.',
                required: false,
                schema: new Schema(
                    type: 'boolean'
                ),
            ),
            new Parameter(
                name: 'sweet',
                description: 'Sweet taste property.',
                required: false,
                schema: new Schema(
                    type: 'boolean'
                ),
            ),
            new Parameter(
                name: 'vegan',
                description: 'Vegan property.',
                required: false,
                schema: new Schema(
                    type: 'boolean'
                ),
            ),
            new Parameter(
                name: 'vegetarian',
                description: 'Vegetarian property.',
                required: false,
                schema: new Schema(
                    type: 'boolean'
                ),
            ),
        ],
        responses: [
            new \OpenApi\Attributes\Response(
                response: 200,
                description: 'Pizza record updated.',
                content: new JsonContent(
                    ref: new Model(
                        type: Pizza::class
                    ),
                    type: 'object',
                ),
            ),
            new \OpenApi\Attributes\Response(
                response: 400,
                description: 'Pizza record data validation error.',
                content: new JsonContent(
                    type: 'object',
                ),
            ),
            new \OpenApi\Attributes\Response(
                response: 401,
                description: 'Unauthorized if empty or wrong token.',
                content: new JsonContent(
                    type: 'object',
                ),
            ),
            new \OpenApi\Attributes\Response(
                response: 404,
                description: 'Pizza record with given id not found.',
                content: new JsonContent(
                    type: 'object',
                ),
            ),
            new \OpenApi\Attributes\Response(
                response: 409,
                description: 'Pizza record with same name exists.',
                content: new JsonContent(
                    type: 'object',
                ),
            ),
        ]
    )]
    public function updateAction(Request $request, string $id, ValidatorInterface $validator): Response
    {
        /** @var Pizza $pizza */
        $pizza = $this
            ->entityManager
            ->getRepository(Pizza::class)
            ->find($id);

        if (!$pizza) {
            return $this->handleView(
                $this->view(['err' => 'Pizza not found'], Response::HTTP_NOT_FOUND)
            );
        }

        /** @var Pizza $existingPizza */
        $existingPizza = $this
            ->entityManager
            ->getRepository(Pizza::class)
            ->findOneBy([
                'name' => $request->query->get('name', ''),
            ]);

        if ($existingPizza) {
            return $this->handleView(
                $this->view(['err' => 'Pizza with such name exists.'], Response::HTTP_CONFLICT)
            );
        }

        $pizza
            ->setName($request->query->get('name', $pizza->getName()))
            ->setPrice($request->query->get('price', $pizza->getPrice()))
            ->setGlutenfree($request->query->get('glutenfree', $pizza->isGlutenfree()))
            ->setSpicy($request->query->get('spicy', $pizza->isSpicy()))
            ->setSweet($request->query->get('sweet', $pizza->isSweet()))
            ->setVegan($request->query->get('vegan', $pizza->isVegan()))
            ->setVegetarian($request->query->get('vegetarian', $pizza->isVegetarian()));

        $validationResults = $validator->validate($pizza);
        if (count($validationResults) > 0) {
            return $this->handleView(
                $this->view((string) $validationResults, Response::HTTP_BAD_REQUEST)
            );
        }

        $this->entityManager->flush();

        return $this->handleView(
            $this->view($pizza, Response::HTTP_OK)
        );
    }

    #[Route('/pizza/{id}', name: 'pizza_delete', methods: ['DELETE'])]
    #[Delete(
        description: 'Delete a pizza record by given id.',
        tags: ["Pizza"],
        parameters: [
            new Parameter(
                name: 'token',
                description: 'Access token',
                in: 'query',
                required: true,
                schema: new Schema(
                    type: 'string'
                ),
                example: '3fa'
            ),
        ],
        responses: [
            new \OpenApi\Attributes\Response(
                response: 200,
                description: 'Pizza record deleted.',
                content: new JsonContent(
                    ref: new Model(
                        type: Pizza::class
                    ),
                    type: 'object',
                ),
            ),
            new \OpenApi\Attributes\Response(
                response: 401,
                description: 'Unauthorized if empty or wrong token.',
                content: new JsonContent(
                    type: 'object',
                ),
            ),
            new \OpenApi\Attributes\Response(
                response: 404,
                description: 'Pizza record with given id not found.',
                content: new JsonContent(
                    type: 'object',
                ),
            ),
        ]
    )]
    public function deleteAction(string $id): Response
    {
        $pizza = $this
            ->entityManager
            ->getRepository(Pizza::class)
            ->find($id);

        if (!$pizza) {
            return $this->handleView(
                $this->view(['err' => 'Pizza not found.'], Response::HTTP_NOT_FOUND)
            );
        }

        $this->entityManager->remove($pizza);
        $this->entityManager->flush();

        return $this->handleView(
            $this->view($pizza, Response::HTTP_OK)
        );
    }
}
