<?php

namespace App\Entity;

use App\Repository\PizzaRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PizzaRepository::class)]
class Pizza
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 3)]
    private $name;

    #[ORM\Column(type: 'float')]
    #[Assert\GreaterThan(0)]
    private $price;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $vegan;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $vegetarian;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $glutenfree;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $spicy;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $sweet;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function isVegan(): ?bool
    {
        return $this->vegan;
    }

    public function setVegan(?bool $vegan): self
    {
        $this->vegan = $vegan;

        return $this;
    }

    public function isVegetarian(): ?bool
    {
        return $this->vegetarian;
    }

    public function setVegetarian(?bool $vegetarian): self
    {
        $this->vegetarian = $vegetarian;

        return $this;
    }

    public function isGlutenfree(): ?bool
    {
        return $this->glutenfree;
    }

    public function setGlutenfree(?bool $glutenfree): self
    {
        $this->glutenfree = $glutenfree;

        return $this;
    }

    public function isSpicy(): ?bool
    {
        return $this->spicy;
    }

    public function setSpicy(?bool $spicy): self
    {
        $this->spicy = $spicy;

        return $this;
    }

    public function isSweet(): ?bool
    {
        return $this->sweet;
    }

    public function setSweet(?bool $sweet): self
    {
        $this->sweet = $sweet;

        return $this;
    }
}
