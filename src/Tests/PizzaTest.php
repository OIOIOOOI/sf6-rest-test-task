<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Tests some pizza api methods.
 */
class PizzaTest extends WebTestCase
{
    protected $client;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * Tests pizza records list.
     *
     * @return void
     */
    public function testInitial(): void
    {
        $this->client->request('GET', '/api/pizza?token=3fa');

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertEmpty(json_decode($this->client->getResponse()->getContent(), true));
    }

    /**
     * Test pizza record creation.
     *
     * @return void
     */
    public function testInsert(): void
    {
        $this->client->request('POST', '/api/pizza?name=Fungi&price=10&token=3fa');

        $this->assertEquals(Response::HTTP_CREATED, $this->client->getResponse()->getStatusCode());

        $pizzaRecord = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('id', $pizzaRecord);
        $this->assertArrayHasKey('name', $pizzaRecord);

        $id = $pizzaRecord['id'];

        $this->client->request('GET', '/api/pizza?token=3fa');
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $pizzaRecords = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertNotEmpty($pizzaRecords);

        $this->client->request('GET', '/api/pizza/' . $id . '?token=3fa');
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $pizzaRecord = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('Fungi', $pizzaRecord['name']);
        $this->assertEquals('10', $pizzaRecord['price']);
    }

    /**
     * Tests pizza record deletion.
     *
     * @return void
     */
    public function testDelete(): void
    {
        $this->client->request('POST', '/api/pizza?name=Fungi&price=10&token=3fa');

        $this->assertEquals(Response::HTTP_CREATED, $this->client->getResponse()->getStatusCode());

        $pizzaRecord = json_decode($this->client->getResponse()->getContent(), true);
        $id = $pizzaRecord['id'];

        $this->client->request('DELETE', '/api/pizza/' . $id . '?token=3fa');
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $this->client->request('GET', '/api/pizza/' . $id . '?token=3fa');
        $this->assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Tests token authentication.
     *
     * @return void
     */
    public function tokenTest(): void
    {
        $this->client->request('POST', '/api/pizza');
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());

        $this->client->request('POST', '/api/pizza?token=3fc');
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());

        $this->client->request('POST', '/api/pizza?token=3fa');
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }
}
