# Symfony 6 REST API test task

Small web project that wraps up some aspects of building
a fully functional REST API using PHP and Symfony 6.

## About
A Symfony 6 based project that uses:
- FOSRestBundle
- NelmioApiDoc for auto-generated documentation
- Doctrine entities
- Data validation
- Basic api key authentication mechanism
- UnitTest (WebTestCase) sample
- PHP Attributes

## Requirements
- PHP8.1
- Apache2 (PHP-FPM or MOD-PHP)
- Mysql 5.7 or above
- composer

## Installation
1. Clone the sources
2. Run `composer install` in root directory
3. Create mysql database (make two, one suffixed with `_test`) and underlying user
4. Edit the `.env.test` and `.env` (or `.env.local.php`) for
proper database credentials.
5. Run `php bin/console doctrine:database:create`
6. Run `php bin/console doctrine:schema:create`
7. Create a virtual host pointing to `public` directory

## Running tests
In root directory run `php ./vendor/bin/phpunit`
